import { Flex } from '@chakra-ui/react';
import { useEffect } from 'react';
import { DataTable } from '../components/DataTable';
import { Filter } from '../components/Filter';
import { useStore } from '../store/PaperStore';

export const Data = () => {
  const fetchFilterCriteria = useStore((state) => state.fetchFilterCriteria);
  const clearFilterCriteria = useStore((state) => state.clearFilterCriteria);
  const fetchPapersByFilterCriteria = useStore(
    (state) => state.fetchPapersByFilterCriteria
  );

  useEffect(() => {
    clearFilterCriteria();
    fetchFilterCriteria();
    fetchPapersByFilterCriteria();
  }, [clearFilterCriteria, fetchFilterCriteria, fetchPapersByFilterCriteria]);

  return (
    <Flex
      justifyContent="stretch"
      alignItems="start"
      style={{ backgroundColor: '#111312' }}
    >
      <Filter />
      <DataTable />
    </Flex>
  );
};
