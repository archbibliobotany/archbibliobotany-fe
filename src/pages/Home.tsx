import { Box, HStack, Image, Link, Text } from '@chakra-ui/react';
import AG_Leventis from '../assets/AGLeventis_Logo.jpg';
import MetaMobilityLogo from '../assets/MetaLogo.png';
import backgroundImg from '../assets/background.jpg';
import CyprusInstitute from '../assets/cyprus-institute.svg';
import David_prize from '../assets/david_prize.png';
import RIF from '../assets/logos.png';

export const Home = () => {
  return (
    <Box
      backgroundColor={'#111312'}
      backgroundImage={`url(${backgroundImg})`}
      backgroundSize="cover"
      backgroundPosition="center"
    >
      <Box backgroundColor="rgba(0,0,0,0.3)" width="100%" padding={4}>
        <Text
          fontWeight="bold"
          fontSize="6xl"
          color="#E88941"
          fontFamily="Raleway"
          mb="4"
        >
          Welcome to PlantBi(bli)oArch!
        </Text>
        <Text
          fontSize="2xl"
          fontWeight="semibold"
          color="#E88941"
          fontFamily="Raleway"
          mb="2"
        >
          Bibliographic database for Archaeobotanical studies
        </Text>
        <Text
          textAlign="justify"
          fontWeight="semibold"
          fontFamily="Raleway"
          color="#e9e9e9"
        >
          PlantBi(bli)oArch is a bibliographic database focusing on
          archaeobotanical studies in the Central and Eastern Mediterranean as
          well as the Balkans and Middle East; it covers assemblages from
          prehistory to early modern times and is an extension of similar
          initiatives by our team for human bioarchaeological and
          zooarchaeological remains: Bi(bli)oArch (https://www.biblioarch.com/)
          and ZooBi(bli)oArch (https://zoobiblioarch.emmebioarch.com/).
          PlantBi(bli)oArch offers different search criteria, allowing users to
          refine their search based on research theme and region, as well as
          publication date. It emerged from an increasing realization that many
          archaeobotanical studies have been disseminated across national
          archaeology journals, excavation monographs, 'grey' reports, or in the
          form of graduate theses, restricting their discoverability. The
          database currently contains 2,238 titles and will be continuously
          updated. Our team is looking to expand the database through
          community-based contributions. Additional papers, corrections and/or
          omissions can be submitted through our submissions/feedback form or by
          emailing us directly. This database was created in the context of the
          MetaMobility project. This project is implemented under the programme
          of social cohesion “THALIA 2021-2027” cofunded by the European Union,
          through Research and Innovation Foundation (EXCELLENCE/0421/0376).
          Database creation was also supported by the Dan David Prize and the A.
          G. Leventis Chair in Archaeological Sciences.
        </Text>
        <HStack borderRadius={20} justifyContent="space-between" wrap="wrap">
          <Image
            src={MetaMobilityLogo}
            alt="MetaMobility logo"
            h="auto"
            w="240px"
          />
          <Image
            src={David_prize}
            alt="Dan David Prize logo"
            h="auto"
            w="230px"
          />
          <Image src={AG_Leventis} alt="AG Leventis logo" h="auto" w="100px" />
          <Link href="https://www.cyi.ac.cy/">
            <Image
              src={CyprusInstitute}
              w="115px"
              h="auto"
              alt="Cyprus Institute Logo"
            />
          </Link>
          <Image src={RIF} alt="RIF Logo" />
        </HStack>
      </Box>
    </Box>
  );
};
