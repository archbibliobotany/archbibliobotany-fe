import { Flex, Spinner } from '@chakra-ui/react';
import React from 'react';
import { useStore } from '../store/PaperStore';
import { DataItem } from './DataItem';
import { Pagination } from './Pagination';
import { generateTSV, generateJSON, generatePDF } from '../utils/exporters';
import { Paper } from '../store/PaperModel';

interface DataTableProps {}

export const DataTable: React.FC<DataTableProps> = () => {
  const papers = useStore((state) => state.papers);
  const pagination = useStore((state) => state.pagination);
  const isLoading = useStore((state) => state.isLoading);
  const exports = useStore((state) => state.exports);

  const fetchPapersByFilterCriteria = useStore(
    (state) => state.fetchPapersByFilterCriteria
  );
  const setSelectedFilters = useStore((state) => state.setSelectedFilters);
  const addOrRemoveFromExports = useStore(
    (state) => state.addOrRemoveFromExports
  );
  const clearExports = useStore((state) => state.clearExports);
  const isInExports = (paper: Paper) =>
    exports.filter((exported) => exported.title === paper.title).length > 0;

  return (
    <Flex gap={5} padding={4} direction="column">
      {isLoading ? (
        <Spinner size="xl" />
      ) : (
        papers.map((paper) => (
          <DataItem
            key={paper._id}
            paper={paper}
            addOrRemoveFromExports={addOrRemoveFromExports}
            isInExports={isInExports}
          />
        ))
      )}
      <Pagination
        currentPage={pagination.currentPage}
        hasNext={pagination.hasNext}
        hasPrev={pagination.hasPrev}
        totalPages={pagination.totalPages}
        totalDocs={pagination.totalDocs}
        isLoading={isLoading}
        fetchPapersByFilterCriteria={fetchPapersByFilterCriteria}
        setSelectedFilters={setSelectedFilters}
        exports={exports}
        generateJSON={(exports = papers) => {
          generateJSON(exports);
        }}
        generatePDF={(exports = papers) => {
          generatePDF(exports);
        }}
        generateTSV={(exports = papers) => {
          generateTSV(exports);
        }}
        clearExports={clearExports}
      />
    </Flex>
  );
};
