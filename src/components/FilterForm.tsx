import { Button, Flex, Input } from '@chakra-ui/react';
import { Select } from 'chakra-react-select';
import React from 'react';
import { Chronology, Keyword, Region } from '../store/PaperModel';
import { PaperStore } from '../store/PaperStore';

interface FilterFormProps {
  chronology: Array<Chronology>;
  region: Array<Region>;
  keywords: Array<Keyword>;
  isLoading: boolean;
  setSelectedFilters: PaperStore['setSelectedFilters'];
  fetchPapersByFilterCriteria: PaperStore['fetchPapersByFilterCriteria'];
  resetPage: () => void;
}

const chronologySortingFunction = (a: Chronology, b: Chronology) => {
  return Number.parseInt(a.name) - Number.parseInt(b.name);
};

export const FilterForm: React.FC<FilterFormProps> = ({
  chronology,
  keywords,
  region,
  isLoading,
  setSelectedFilters,
  fetchPapersByFilterCriteria,
  resetPage,
}) => {
  // Define custom styles for the placeholder color
  const customStyles = {
    control: (styles: any) => ({
      ...styles,
      backgroundColor: '#111312', // Set the control's background to transparent
    }),
    placeholder: (styles: any) => ({
      ...styles,
      color: '#111312', // Set the placeholder color to #111312
    }),
  };

  return (
    <Flex
      direction="column"
      grow="0"
      minW="25%"
      justifyContent="center"
      p={6}
      gap={6}
    >
      {/* <Select
        isLoading={isLoading}
        selectedOptionStyle="check"
        isMulti
        placeholder="Chronology"
        options={[
          ...chronology
            .filter(
              (elm) => !elm.name.endsWith(' CE') && !elm.name.endsWith('BCE')
            )
            .sort((a, b) => (a.name >= b.name ? 1 : -1)),
          ...chronology
            .filter((elm) => elm.name.endsWith('BCE'))
            .sort(chronologySortingFunction),
          ...chronology
            .filter((elm) => elm.name.endsWith(' CE'))
            .sort(chronologySortingFunction),
        ].map((chronology) => ({
          label: chronology.name,
          value: chronology._id,
        }))}
        onChange={(chosenChronologys) => {
          setSelectedFilters({
            chronology: chosenChronologys.map((chosenChronology) => ({
              _id: chosenChronology.value,
            })),
          });
        }}
      /> */}
      <Select
        isLoading={isLoading}
        selectedOptionStyle="check"
        isMulti
        placeholder="Keywords"
        options={keywords
          .map((keyword) => ({
            label: keyword.name,
            value: keyword._id,
          }))
          .sort((a, b) => (a.label > b.label ? 1 : -1))}
        onChange={(chosenKeywords) => {
          setSelectedFilters({
            keywords: chosenKeywords.map((chosenKeyword) => ({
              _id: chosenKeyword.value,
            })),
          });
        }}
      />
      <Select
        isLoading={isLoading}
        selectedOptionStyle="check"
        isMulti
        placeholder="Regions"
        options={region
          .map((region) => ({
            label: region.name,
            value: region._id,
          }))
          .sort((a, b) => (a.label > b.label ? 1 : -1))}
        onChange={(chosenRegions) => {
          setSelectedFilters({
            regions: chosenRegions.map((chosenRegion) => ({
              _id: chosenRegion.value,
            })),
          });
        }}
      />
      <Input
        disabled={isLoading}
        type="number"
        color="#fff"
        placeholder="Year From"
        onChange={(e) => {
          setSelectedFilters({ yearFrom: e.target.valueAsNumber || undefined });
        }}
      />
      <Input
        disabled={isLoading}
        type="number"
        color="#fff"
        placeholder="Year To"
        onChange={(e) => {
          setSelectedFilters({ yearTo: e.target.valueAsNumber || undefined });
        }}
      />
      <Button
        disabled={isLoading}
        background="#E88941"
        onClick={() => {
          resetPage();
          fetchPapersByFilterCriteria();
        }}
      >
        Submit
      </Button>
    </Flex>
  );
};
