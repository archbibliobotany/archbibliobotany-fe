import { Box } from '@chakra-ui/react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import { Data } from '../pages/Data';
import { Home } from '../pages/Home';
import { Team } from '../pages/Team';
import { Header } from './Header';

export const Router = () => (
  <Box backgroundColor="rgb(17, 19, 18)" width="100%" height="100%">
    <MemoryRouter>
      <Header />
      <Routes>
        <Route index element={<Home />} />
        <Route path="/data" element={<Data />} />
        <Route path="/team" element={<Team />} />
      </Routes>
    </MemoryRouter>
  </Box>
);
