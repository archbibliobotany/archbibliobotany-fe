import React from 'react';
import { useStore } from '../store/PaperStore';
import { FilterForm } from './FilterForm';

export const Filter: React.FC = () => {
  const chronology = useStore((state) => state.chronology);
  const keywords = useStore((state) => state.keywords);
  const regions = useStore((state) => state.regions);
  const isLoading = useStore((state) => state.isLoading);

  const setSelectedFilters = useStore((state) => state.setSelectedFilters);
  const resetPage = useStore((state) => state.resetPage);
  const fetchPapersByFilterCriteria = useStore(
    (state) => state.fetchPapersByFilterCriteria
  );

  return (
    <FilterForm
      chronology={chronology}
      keywords={keywords}
      region={regions}
      isLoading={isLoading}
      setSelectedFilters={setSelectedFilters}
      fetchPapersByFilterCriteria={fetchPapersByFilterCriteria}
      resetPage={resetPage}
    />
  );
};
