import create from 'zustand';
import { Chronology, Keyword, Paper, Region } from './PaperModel';
import superagent from 'superagent';
import BASE_URL from '../URL_CONFIG';

export interface Filters {
  regions?: Partial<Region>[];
  chronology?: Partial<Chronology>[];
  keywords?: Partial<Keyword>[];
  yearFrom?: number;
  yearTo?: number;
  page?: number;
  pageSize?: number;
}

export interface Pagination {
  currentPage: number;
  hasNext: boolean;
  hasPrev: boolean;
  totalDocs: number;
  totalPages: number;
}

export interface PaperStore {
  papers: Paper[];
  regions: Region[];
  chronology: Chronology[];
  keywords: Keyword[];
  pagination: Pagination;
  isLoading: boolean;
  selectedFilters: Filters;
  exports: Paper[];
  fetchFilterCriteria: () => void;
  fetchPapersByFilterCriteria: () => void;
  setSelectedFilters: (filters: Filters) => void;
  clearFilterCriteria: () => void;
  resetPage: () => void;
  addOrRemoveFromExports: (paper: Paper) => void;
  clearExports: () => void;
}

interface PaginatedResponse {
  docs: Array<Paper>;
  totalDocs: number;
  currentPage: number;
  hasPrev: boolean;
  hasNext: boolean;
  totalPages: number;
}

export const useStore = create<PaperStore>()((set, get) => ({
  authors: [],
  regions: [],
  chronology: [],
  keywords: [],
  papers: [],
  pagination: {
    currentPage: 0,
    hasNext: false,
    hasPrev: false,
    totalDocs: 0,
    totalPages: 0,
  },
  selectedFilters: {
    yearFrom: undefined,
    yearTo: undefined,
    chronology: [],
    keywords: [],
    regions: [],
    page: 0,
    pageSize: 10,
  },
  exports: [],
  isLoading: false,
  fetchFilterCriteria: () => {
    set({ isLoading: true });
    superagent.get(BASE_URL + 'get').end((error, response) => {
      if (error) throw new Error(`ERROR: ${error}`);

      const { regions, chronology, keywords } = response.body as Partial<PaperStore>;

      set((paperStore) => ({
        ...paperStore,
        chronology,
        keywords,
        regions,
        isLoading: false,
      }));
    });
  },
  fetchPapersByFilterCriteria: () => {
    const superagentRequest = superagent.post(BASE_URL + 'post');
    set({ isLoading: true });

    const { chronology, keywords, regions, yearFrom, yearTo, page, pageSize } = get().selectedFilters;

    superagentRequest.send({
      chronology,
      keywords,
      regions,
      yearFrom,
      yearTo,
      page,
      pageSize,
    });

    superagentRequest.end((error, response) => {
      if (error) throw new Error(`ERROR: ${error}`);

      const { docs, currentPage, hasNext, hasPrev, totalDocs, totalPages } =
        response.body as PaginatedResponse;

      set((paperStore) => ({
        ...paperStore,
        papers: docs,
        pagination: {
          currentPage,
          hasNext,
          hasPrev,
          totalDocs,
          totalPages,
        },
        isLoading: false,
      }));
    });
  },
  setSelectedFilters: (filters) => {
    set((paperStore) => ({
      selectedFilters: { ...paperStore.selectedFilters, ...filters },
    }));
  },
  clearFilterCriteria: () => {
    set((papersStore) => ({
      ...papersStore,
      selectedFilters: {
        yearFrom: undefined,
        yearTo: undefined,
        chronology: [],
        keywords: [],
        regions: [],
        page: 0,
        pageSize: 10,
      },
    }));
  },
  resetPage: () => {
    set((papersStore) => ({
      ...papersStore,
      selectedFilters: {
        ...papersStore.selectedFilters,
        page: 0,
      },
    }));
  },
  addOrRemoveFromExports: (paper: Paper) => {
    const filteredExports = get().exports.filter(
      (exported) => exported.title !== paper.title
    );
    const isInExports = filteredExports.length !== get().exports.length;

    if (!isInExports)
      set((paperStore) => ({
        ...paperStore,
        exports: [...get().exports, paper],
      }));
    else
      set((paperStore) => ({
        ...paperStore,
        exports: filteredExports,
      }));
  },
  clearExports: () => {
    set((paperStore) => ({ ...paperStore, exports: [] }));
  },
}));
