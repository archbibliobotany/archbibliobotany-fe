export type Keyword = {
  _id: string;
  name: string;
};
export type Chronology = {
  _id: string;
  name: string;
};
export type Region = {
  _id: string;
  name: string;
};
export type Paper = {
  _id: string;
  title: string;
  year: number;
  authors: string;
  doi: string;
  regions: Array<Region>;
  chronology: Array<Chronology>;
  keywords: Array<Chronology>;
  journal_book: string;
  abstract: string;
};
