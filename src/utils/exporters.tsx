import { Paper } from '../store/PaperModel.js';

export function download(filename: string, text: string) {
  var element = document.createElement('a');
  element.setAttribute(
    'href',
    'data:text/tab-separated-values;charset=utf-8,' + encodeURIComponent(text)
  );
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

const keys = [
  'Authors',
  'Title',
  'Year',
  'Chronology',
  'Keywords',
  'Regions',
  'Journal/Book',
  'DOI',
  'Abstract',
];

export const generateTSV = (exports: Paper[]) => {
  if (exports.length === 0) {
    return;
  }
  let tsv = exports.map(function (row) {
    return [
      row.authors,
      row.title,
      row.year,
      row.chronology.map((elm) => elm.name).join(', '),
      row.keywords.map((elm) => elm.name).join(', '),
      row.regions.map((elm) => elm.name).join(', '),
      row.journal_book,
      row.doi,
      row.abstract,
    ]
      .map((elm) => elm?.toString().replaceAll('\n', '').replaceAll('\t', ''))
      .join('\t');
  });
  tsv.unshift(keys.join('\t')); // add header column
  const out = tsv.join('\r\n');
  download(`data${Math.trunc(Math.random() * 10000)}.tsv`, out);
};

export const generatePDF = (exports: Paper[]) => {
  import('./pdfUtil.js')
    .then((pdfMake) => {
      // @ts-ignore
      pdfMake = pdfMake.default;
      const content = exports.map((row) => {
        let item = [
          row.authors,
          row.title,
          row.year,
          row.chronology.map((elm) => elm.name).join(', '),
          row.keywords.map((elm) => elm.name).join(', '),
          row.regions.map((elm) => elm.name).join(', '),
          row.journal_book,
          row.doi,
          row.abstract,
        ];
        let line = item.map((val, idx) => {
          return `${keys[idx]}: ${val ? val : ''}`;
        });

        return [line, ' '];
      });

      const docDefinition = {
        content,
      };
      // @ts-ignore
      pdfMake.createPdf(docDefinition).download();
    })
    .catch((exception) => {
      console.error(exception);
    });
};

export const generateJSON = (exports: Paper[]) => {
  download(
    `data${Math.trunc(Math.random() * 10000)}.json`,
    JSON.stringify(exports)
  );
};
